#!/usr/bin/env bash


#################################################
# Master install script.                        #
#                                               #
# This will run all individual install scripts  #
# to set up the system as desired.              #
#################################################

source ./color.sh

run_script () { # $1 is the command; $2 is the message
    echo -e "${BoldGreen}Running $2${ResetColor}"
    $1
    if [ "$?" -ne 0 ]
    then
        echo -e "${BoldRed}☠ Error with $2${ResetColor}: ${1}"
        exit 1
    fi
    echo -e "${BoldGreen}✔ Finished $2${ResetColor}"
}

# Initial apt packages
run_script ./apt_packages.sh "initial install of apt packages"

# Install Starship
run_script ./starship.sh "install of Starship command line prompt"

# Cleanup
run_script ./cleanup.sh "post install cleanup"

echo -e "${BoldGreen}✔✔✔ Finished with all setup/install scripts ✔✔✔${ResetColor}"