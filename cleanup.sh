#!/usr/bin/env bash


#################################################
# Finish install by cleaning up                 #
#################################################

source ./color.sh

run_command () { # $1 is the command; $2 is the message
    echo -e "  ${BoldCyan}Running $2${ResetColor}"
    $1
    if [ "$?" -ne 0 ]
    then
        echo -e "  ${BoldRed}☠ Error with $2${ResetColor}: ${1}"
        exit 1
    fi
    echo -e "  ${BoldCyan}✔ Finished $2${ResetColor}"
}

# Remove unneeded packages
run_command "sudo apt-get -y autoremove" "apt package autoremove"
