#!/usr/bin/env bash


#################################################
# Install apt packages                          #
#################################################

source ./color.sh

run_command () { # $1 is the command; $2 is the message
    echo -e "  ${BoldCyan}Running $2${ResetColor}"
    $1
    if [ "$?" -ne 0 ]
    then
        echo -e "  ${BoldRed}☠ Error with $2${ResetColor}: ${1}"
        exit 1
    fi
    echo -e "  ${BoldCyan}✔ Finished $2${ResetColor}"
}

# Initial apt packages
run_command "sudo apt-get install -y curl mc" "apt package install"
