#!/usr/bin/env bash


#################################################
# Install the starship command line prompt      #
#################################################

source ./color.sh

run_command () { # $1 is the command; $2 is the message
    echo -e "  ${BoldCyan}Running $2${ResetColor}"
    eval $1
    if [ "$?" -ne 0 ]
    then
        echo -e "  ${BoldRed}☠ Error with $2${ResetColor}: ${1}"
        exit 1
    fi
    echo -e "  ${BoldCyan}✔ Finished $2${ResetColor}"
}

# Install Starship command line prompt
run_command "curl -sS https://starship.rs/install.sh | sh" "install starship"

run_command 'echo -e "\neval \"\$(starship init bash)\"\n" >> ~/.bashrc' "load starship into bashrc"