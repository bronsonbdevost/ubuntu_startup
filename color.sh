#!/usr/bin/env bash


####################################################################################
# Colorize shell output with simple vars.                                          #
#                                                                                  #
# Taken from:                                                                      #
# https://stackoverflow.com/questions/16843382/colored-shell-script-output-library #
####################################################################################

## Enable our easy to read Colour Flags as long as --no-colors hasn't been passed or the NO_COLOR Env Variable is set. 
## NOTE: the NO_COLOR env variable is from: https://no-color.org/
if [[ ! $* == *--no-color* && -z "${NO_COLOR}" ]]
then 

    ESeq="\x1b["

    # Set up our Colour Holders. 
    ResetColor="$ESeq"'0m'    # Text Reset

    Bold="$ESeq"'1m';    Underline="$ESeq"'4m';    Blinking="$ESeq"'5m';     Reversed="$ESeq"'7m'

    # Regular              Bold                        Underline                       High Intensity           
    Black="$ESeq"'0;30m';  BoldBlack="$ESeq"'1;30m';   UnderlineBlack="$ESeq"'4;30m';  IntenseBlack="$ESeq"'0;90m'; 
    Red="$ESeq"'0;31m';    BoldRed="$ESeq"'1;31m';     UnderlineRed="$ESeq"'4;31m';    IntenseRed="$ESeq"'0;91m';   
    Green="$ESeq"'0;32m';  BoldGreen="$ESeq"'1;32m';   UnderlineGreen="$ESeq"'4;32m';  IntenseGreen="$ESeq"'0;92m'; 
    Yellow="$ESeq"'0;33m'; BoldYelllow="$ESeq"'1;33m'; UnderlineYellow="$ESeq"'4;33m'; IntenseYellow="$ESeq"'0;93m';
    Blue="$ESeq"'0;34m';   BoldBlue="$ESeq"'1;34m';    UnderlineBlue="$ESeq"'4;34m';   IntenseBlue="$ESeq"'0;94m';  
    Purple="$ESeq"'0;35m'; BoldPurple="$ESeq"'1;35m';  UnderlinePurple="$ESeq"'4;35m'; IntensePurple="$ESeq"'0;95m';
    Cyan="$ESeq"'0;36m';   BoldCyan="$ESeq"'1;36m';    UnderlineCyan="$ESeq"'4;36m';   IntenseCyan="$ESeq"'0;96m';  
    White="$ESeq"'0;37m';  BoldWhite="$ESeq"'1;37m';   UnderlineWhite="$ESeq"'4;37m';  IntenseWhite="$ESeq"'0;97m'; 

    #Bold High Intensity                Background              High Intensity Backgrounds
    BoldIntenseBlack="$ESeq"'1;90m';    OnBlack="$ESeq"'40m';   OnIntenseBlack="$ESeq"'0;100m';
    BoldIntenseRed="$ESeq"'1;91m';      OnRed="$ESeq"'41m';     OnIntenseRed="$ESeq"'0;101m';
    BoldIntenseGreen="$ESeq"'1;92m';    OnGreen="$ESeq"'42m';  OnIntenseGreen="$ESeq"'0;102m';
    BoldIntenseYellow="$ESeq"'1;93m';   OnYellow="$ESeq"'43m';  OnIntenseYellow="$ESeq"'0;103m';
    BoldIntenseBlue="$ESeq"'1;94m';     OnBlue="$ESeq"'44m';    OnIntenseBlue="$ESeq"'0;104m';
    BoldIntensePurple="$ESeq"'1;95m';   OnPurple="$ESeq"'45m';  OnIntensePurple="$ESeq"'0;105m';
    BoldIntenseCyan="$ESeq"'1;96m';     OnCyan="$ESeq"'46m';    OnIntenseCyan="$ESeq"'0;106m';
    BoldIntenseWhite="$ESeq"'1;97m';    OnWhite="$ESeq"'47m';   OnIntenseWhite="$ESeq"'0;107m';

    #Blinking                        Reversed             
    BlinkingBlack="$ESeq"'5;30m';    ReversedBlack="$ESeq"'7;30m';
    BlinkingRed="$ESeq"'5;31m';      ReversedRed="$ESeq"'7;31m';
    BlinkingGreen="$ESeq"'5;32m';    ReversedGreen="$ESeq"'7;32m';
    BlinkingYellow="$ESeq"'6;33m';   ReversedYellow="$ESeq"'7;33m';
    BlinkingBlue="$ESeq"'5;34m';     ReversedBlue="$ESeq"'7;34m';
    BlinkingPurple="$ESeq"'5;35m';   ReversedPurple="$ESeq"'7;35m';
    BlinkingCyan="$ESeq"'5;36m';     ReversedCyan="$ESeq"'7;36m';
    BlinkingWhite="$ESeq"'5;37m';    ReversedWhite="$ESeq"'7;37m';
fi